//Js for the page search.
document.getElementById('block-serchimage').onclick = function() {
  let search = document.getElementsByClassName('search-region')[0];
  document.body.classList.add('fixed');
  search.classList.add('active');
  let lang = document.documentElement.lang;
  if (lang === "ar") {
    document.getElementById('edit-keys').placeholder = "البحث هنا";
  } else {
    document.getElementById('edit-keys').placeholder = "Search here...";
  }
}
document.getElementById('block-closebutton').onclick = function() {
  let search = document.getElementsByClassName('search-region')[0];
  document.body.classList.remove('fixed');
  search.classList.remove('active');
}

//Code for the hidden joke.
if (document.body.classList.contains('mros')) {
  let thanks = document.getElementById('shhh');
  thanks.onclick = function () {
    let dialog = document.getElementsByClassName("dialog-off-canvas-main-canvas")[0];
    dialog.classList.toggle("active");
  }
}

function insta() {
  if (document.getElementsByClassName('eapps-link')[0] !== undefined) {
    document.getElementsByClassName('eapps-link')[0].style.display = 'none';
  }
}
setTimeout(insta, 2500);

toggler = document.getElementsByClassName('navbar-toggler')[0];
toggler.onclick = function () {
  let accordion_passenger = document.getElementById('superfish-passengers-menu-accordion');
  let accordion_corporate = document.getElementById('superfish-corporate-menu-accordion');

  if (accordion_passenger !== null) {
    document.getElementById('superfish-passengers-menu-toggle').classList.toggle('sf-expanded');
    if (accordion_passenger.classList.contains("sf-hidden")) {
      accordion_passenger.classList.remove("sf-hidden");
      accordion_passenger.classList.add("sf-expanded");
      accordion_passenger.style.display = "block";
      document.body.classList.add("fixed-body");
    } else {
      accordion_passenger.classList.remove("sf-expanded");
      accordion_passenger.classList.add("sf-hidden");
      accordion_passenger.style.display = "none";
      document.body.classList.remove("fixed-body");
    }
  }

  if (accordion_corporate !== null) {
    document.getElementById('superfish-corporate-menu-toggle').classList.toggle('sf-expanded');
    if (accordion_corporate.classList.contains("sf-hidden")) {
      accordion_corporate.classList.remove("sf-hidden");
      accordion_corporate.classList.add("sf-expanded");
      accordion_corporate.style.display = "block";
      document.body.classList.add("fixed-body");
    } else {
      accordion_corporate.classList.remove("sf-expanded");
      accordion_corporate.classList.add("sf-hidden");
      accordion_corporate.style.display = "none";
      document.body.classList.remove("fixed-body");
    }
  }
}
