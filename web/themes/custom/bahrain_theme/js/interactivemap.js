(function ($, Drupal) {
  $shopping = '/themes/custom/bahrain_theme/img/shopping.png';
  $dinning = '/themes/custom/bahrain_theme/img/restaurant-dinner.png';
  $services = '/themes/custom/bahrain_theme/img/services.png';
  $lounges = '/themes/custom/bahrain_theme/img/hotel-lounges.png'
  $(window).on("load", function() {
    $('#tooltip-icon-41, #tooltip-icon-34, #tooltip-icon-38, #tooltip-icon-33, #tooltip-icon-32, #tooltip-icon-56, #tooltip-icon-46, #tooltip-icon-47, #tooltip-icon-48, #tooltip-icon-55, #tooltip-icon-45, #tooltip-icon-156').attr('src',$shopping);
    $('#tooltip-icon-12, #tooltip-icon-14, #tooltip-icon-13, #tooltip-icon-15').attr('src',$dinning);
    $('#tooltip-icon-91, #tooltip-icon-92, #tooltip-icon-88, #tooltip-icon-89, #tooltip-icon-87').attr('src',$lounges);
    $('#tooltip-icon-129, #tooltip-icon-117, #tooltip-icon-120, #tooltip-icon-120, #tooltip-icon-118, #tooltip-icon-117').attr('src',$services);
  });
})(jQuery, Drupal);
