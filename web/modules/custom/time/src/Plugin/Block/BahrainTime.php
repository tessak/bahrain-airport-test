<?php

namespace Drupal\time\Plugin\Block;

use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Bahrain Time' Block.
 *
 * @Block(
 *   id = "bahrain_time",
 *   admin_label = @Translation("Bahrain Time"),
 *   category = @Translation("Time"),
 * )
 */
class BahrainTime extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#attached' => [
        'library' => [
          'time/time_library',
        ],
      ],
    ];
  }
}
