function getDate()
{
  var date = new Date();
  var hours =  date.getHours();
  var minutes = date.getMinutes();
  var seconds = date.getSeconds();
  if (hours>=24) {
    hours = hours - 24;
  }
  for(var i = 0; i<10; i++){
    if(hours==i){
      hours='0'+hours;
    }
    if(minutes==i){
      minutes='0'+minutes;
    }
    if(seconds==i){
      seconds='0'+seconds;
    }
  }
  if (document.documentElement.lang === "en") {document.getElementById('block-bahraintime').innerHTML = 'Bahrain&nbsp;' + hours + ':' + minutes + ':' + seconds;}
  if (document.documentElement.lang === "ar") {document.getElementById('block-bahraintime').innerHTML = 'لبحرين&nbsp;&nbsp;' + hours + ':' + minutes + ':' + seconds;}
}
getDate();
setInterval(getDate, 1000);
