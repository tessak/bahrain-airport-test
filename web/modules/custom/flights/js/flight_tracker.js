const map = L.map('flights-map').setView([30, 60], 3.5);
const attributtion = 'bj7hg';
const tileUrl = 'https://tile.thunderforest.com/cycle/{z}/{x}/{y}.png?apikey=82a1d2065df544738e10bffa97b03d1b';
const tiles = L.tileLayer(tileUrl, {attributtion, maxZoom: 12, minZoom: 4});
tiles.addTo(map);
const api_url = 'https://raw.githubusercontent.com/bj7hg/bj7hg.github.io/main/planes.json';

// Adjusting markers.
const planeIcon = L.icon({
  iconUrl: 'https://bit.ly/3usy63s',
  iconSize: [32, 80]
});

// We get an icon for the airport.
const airportIcon = L.icon({
  iconUrl: 'https://bit.ly/39TeTP1',
  iconSize: [48, 105]
});

const moscowIcon = L.icon({
  iconUrl: 'https://bj7hg.github.io/img/bomb-icon.png',
  iconSize: [48, 60]
});

// Coordinates of the airport.
const airportLocation = [26.270141979268757, 50.634467926988556];
const moscow = [55.699236, 37.635684];
// We get data from the server and render the movement of aircraft.
async function getFlights() {
  const response = await fetch(api_url);
  let data = await response.json();
  console.log(data);
  for (let i = 0; i < data.planes.length; i++) {
    let {flight_number, origin, time, airline_logo, airline, status, flight_status, longitude, latitude, direction} = data.planes[i];
    let plane = data.planes[i];
    // When you click the plane, we get information about the flight.
    let customPopup =
      `<img width= 50 src= "${airline_logo}" alt="logo">`
      + '<h3>' + "Origin: " + origin + '</h3>'
      + '<p>' + "Estimated Time: " + `<span class="time">` + time + `</span>` + '</p>'
      + '<div class="status">' + status + '</div>';
    if (status !== "On time") {
        continue;
    }
    // Add the plane to the map according to its coordinates.
    const marker = L.marker([latitude, longitude])
      .bindPopup(customPopup, {keepInView: true})
      .addTo(map);
    marker.setIcon(planeIcon);
    marker.setRotationAngle(direction);
    // When you click on the airport icon, you get its description.
    const airportPopup = L.popup({
      closeOnClick: true
    }).setContent(`
        <h2 style = 'text-align: center'>Bahrain International Airport</h2>
        <p style = 'text-align: center'>Rd 2404, Muharraq</p>
        <p style = 'text-align: center'>${data.planes.length} planes are being serviced rn!</p>
        `);

    const airportMarker = L.marker(airportLocation, {icon: airportIcon})
      .addTo(map).bindPopup(airportPopup).addEventListener('click', () => {
        const {_latlng: {lat, lng}} = airportMarker;
        map.setView([lat, lng]);
      });

    const moscowPopup = L.popup({
      closeOnClick: true
    }).setContent(`
        <h2 style = 'text-align: center'>Слава Україні!</h2>
        `);

    const moscowMarker = L.marker(moscow, {icon: moscowIcon})
      .addTo(map).bindPopup(moscowPopup).addEventListener('click', () => {
        const {_latlng: {lat, lng}} = moscowMarker;
        map.setView([lat, lng]);
      });

    let movePlanes = function() {
      if (direction < 90 && direction > 0 ) {
        latitude = parseFloat(latitude) + 0.002 ;
        longitude = parseFloat(longitude) + 0.002 ;
      } else if (direction < 180 && direction > 90 ) {
        latitude = parseFloat(latitude) - 0.002;
        longitude = parseFloat(longitude) + 0.002;
      } else if (direction < 270 && direction > 180 ) {
        latitude = parseFloat(latitude) - 0.002;
        longitude = parseFloat(longitude) - 0.002;
      } else if (direction < 360 && direction > 270 ) {
        latitude = parseFloat(latitude) + 0.002;
        longitude = parseFloat(longitude) - 0.002;
      }

      function changeCoordinates() {
        marker.setLatLng([latitude, longitude]);
      }
      changeCoordinates();
    };
    setTimeout(movePlanes, 0);
    setInterval(movePlanes, 1000);
  }

}
getFlights();

