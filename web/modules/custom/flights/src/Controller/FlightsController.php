<?php

namespace Drupal\flights\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\flights\Entity\Flight;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for flights routes.
 */
class FlightsController extends ControllerBase {

  /**
   * Drupal services.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityManager;

  /**
   * Method provide dependency injection and add services.
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * Builds the response.
   * @throws EntityStorageException
   */
  public function build() {
    $this->convertArray();
    return [];
  }

  /**
   * A method that converts json data to an array.
   *
   * Then we create entity objects from that data.
   * @throws EntityStorageException
   */
  public function convertArray() {
    $json_data = file_get_contents('https://raw.githubusercontent.com/bj7hg/bj7hg.github.io/main/planes.json');
    // Decodes the JSON.
    $array = json_decode($json_data, TRUE);
    $storage = $this->entityManager->getStorage('flight');
    // We get id.
    $flights_ids = $storage->getQuery()->execute();
    // We have created an array that we will fill with flight numbers.
    $all_numbers = [];
    // We go through the numbers of planes and fill in the array.
    foreach ($flights_ids as $id) {
      $flight_data = $storage->load($id);
      $number_flight = $flight_data->get('number')->value;
      $all_numbers[] = $number_flight;
    }

    // A loop that sorts through an array of flight data
    // and creates entity objects.
    for ($i = 0; $i < count($array['planes']); $i++) {
      $time_flight = strtotime(date($array['planes'][$i]['time']));
      // If the flight number is not in the database,
      // we will create the object of this flight.
      if (!in_array($array['planes'][$i]['flight_number'], $all_numbers)) {
        $flight_data = Flight::create([
          'number' => $array['planes'][$i]['flight_number'],
          'origin' => $array['planes'][$i]['origin'],
          'time' => $time_flight,
          'airline' => $array['planes'][$i]['airline'],
          'uri' => $array['planes'][$i]['airline_logo'],
          'status_plane' => $array['planes'][$i]['status'],
          'flight_status' => $array['planes'][$i]['flight-status'],
        ], 'flight');
        $flight_data->save();
      }
    }
  }

}
