<?php

namespace Drupal\flights\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for flights tracker.
 */
class FlightsTrackerController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {
    return [
      '#theme' => 'flight-tracker-page',
    ];
  }

}
