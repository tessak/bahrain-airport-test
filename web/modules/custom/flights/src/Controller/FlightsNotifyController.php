<?php

namespace Drupal\flights\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for flights notify email.
 */
class FlightsNotifyController extends ControllerBase {

  /**
   * Drupal services.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityManager;

  /**
   * Drupal\Core\Form\FormBuilder definition.
   *
   * @var \Drupal\Core\Form\FormBuilder
   */
  protected $formBuilder;

  /**
   * Method provide dependency injection and add services.
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityManager = $container->get('entity_type.manager');
    $instance->formBuilder = $container->get('form_builder');
    return $instance;
  }

  /**
   * Flight id for the modal window.
   *
   * @var int
   */
  public $id;

  /**
   * Builds the response.
   */
  public function build($id = NULL) {
    $this->id = $id;
    $storage = $this->entityManager->getStorage('flight');
    $flights_ids = $storage->getQuery()->execute();
    // We get a form for entering email.
    $form = $this->formBuilder->getForm('Drupal\flights\Form\SendEmailForm');
    // We return the id and flight number.
    $flights_data = [];
    foreach ($flights_ids as $flight_id) {
      $flight_data = $storage->load($id);
      $number = $flight_data->get('number')->value;
      $status_plane = $flight_data->get('status_plane')->value;
      if ($this->id == $flight_id) {
        $render_flight_data = [
          '#theme' => "flights-notify-modal",
          '#id' => $flight_id,
          '#number' => $number,
          '#form' => $form,
          '#status_plane' => $status_plane,
        ];
        $flights_data[] = $render_flight_data;
      }
      else {
        $flights_data[] = NULL;
      }

    }
    return $flights_data;
  }

}

