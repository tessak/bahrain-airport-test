<?php

namespace Drupal\flights\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\HtmlCommand;

/**
 * Form for sending an email.
 */
class SendEmailForm extends FormBase {

  /**
   * The mail manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Renderer.
   *
   * @var \Drupal\Component\Render
   */
  protected $renderer;

  /**
   * Method provide dependency injection and add services.
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->currentUser = $container->get('current_user');
    $instance->messenger = $container->get('messenger');
    $instance->mailManager = $container->get('plugin.manager.mail');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'flights_send_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attached'] = ['library' => ['flights/flights_library']];
    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t("Your email"),
      "#placeholder" => $this->t('Enter your email'),
      "#required" => TRUE,
      '#attributes' => [
        'class' => ['custom-class'],
      ],
      '#suffix' => '<div class="email-validation-message"></div>',
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#name' => 'submit',
      '#value' => $this->t("Register"),
      '#ajax' => [
        'callback' => '::setMessage',
        'progress' => [
          'type' => 'none',
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $renderable = [
      '#theme' => 'email-letter',
      '#username' => $form_state->getValue('email'),
    ];
    $markup = $this->renderer->render($renderable);
    $module = 'flights';
    $key = 'important_event';
    $to = $form_state->getValue('email');
    $params['message'] = $markup;
    $langcode = $this->currentUser()->getPreferredLangcode();
    $send = TRUE;
    $result = $this->mailManager->mail($module, $key, $to, $langcode, $params, NULL, $send);
    if ($result['result'] !== TRUE) {
      $this->messenger()->addStatus($this->t('Invalid'));
    }
  }

  /**
   * Ajax submitting.
   */
  public function setMessage(array &$form, FormStateInterface $form_state): object {
    $response = new AjaxResponse();
    if (!$form_state->hasAnyErrors()) {
      $response->addCommand(
        new HtmlCommand(
          '.email-validation-message',
          '<div class = "valid-email-message">' . $this->t('You have successfully registered.') . '</div>'
        )
      );
      $response->addCommand(new InvokeCommand('.custom-class', 'val', ['']));
    }
    else {
      $response->addCommand(
        new HtmlCommand(
          '.email-validation-message',
          '<div class = "invalid-email-message">' . $this->t('Enter the correct email!') . '</div>'
        )
      );
    }
    $this->messenger->deleteAll();
    return $response;
  }

}