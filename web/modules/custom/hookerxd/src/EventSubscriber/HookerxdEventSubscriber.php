<?php

namespace Drupal\hookerxd\EventSubscriber;

use Drupal\Core\Routing\RouteBuildEvent;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class HookerxdEventSubscriber.
 *
 * @package Drupal\custom_events\EventSubscriber
 */
class HookerxdEventSubscriber implements EventSubscriberInterface {

  /**
   * @return array
   */
  public static function getSubscribedEvents()
  {
    $events[RoutingEvents::ALTER] = 'alterRoutes';
    return $events;
  }

  public function alterRoutes(RouteBuildEvent $event) {
    $collection = $event->getRouteCollection();
    if ($route = $collection->get('field_tooltips_data.tooltip')) {
      $route->setDefault('_controller', '\Drupal\hookerxd\Controller\NewTooltipsDataController::tooltip');
    }
  }
}

