<?php

namespace Drupal\weather\Plugin\Block;

use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Bahrain Time' Block.
 *
 * @Block(
 *   id = "weather_in_bahrain",
 *   admin_label = @Translation("Weather in Bahrain"),
 *   category = @Translation("Weather"),
 * )
 */
class BahrainWeather extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#attached'=> [
        'library' => [
          'weather/weather_library',
        ],
      ],
    ];
  }
}
