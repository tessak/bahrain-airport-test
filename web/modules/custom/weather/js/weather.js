fetch('http://api.openweathermap.org/data/2.5/weather?q=Bahrain&appid=0131c5a9373d24e94a9aafa8089638d8')
  .then(function (resp) {return resp.json() })
  .then(function (data) {
      document.getElementById('block-weatherinbahrain').innerHTML =
      '<img class = "weather_image" src = "https://openweathermap.org/img/wn/' + data.weather[0]['icon'] + '@2x.png">'
      + '<p class = "weather_temperature">' + Math.round(data.main.temp - 273) + ' C&deg; / '
      + Math.round(1.8 * (data.main.temp - 273) + 32) + ' F&deg;' + '</p>';
    })